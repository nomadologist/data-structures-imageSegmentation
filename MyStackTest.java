package project3;

import java.util.EmptyStackException;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit tests for the MyStack class.
 * 
 * @author Jack Booth
 * @version 4/6/2017
 */
public class MyStackTest
{
//	Checks to make sure the constructor properly creates a stack object
	@Test
	public void nullStackTest()
	{
		MyStack<String> testStack = new MyStack<String>();
		if(testStack==null)
			Assert.fail();
	}
	
//	Should create an empty stack of type String, and verify correctly that the stack is empty
	@Test
	public void emptyStringStackTest()
	{
		MyStack<String> testStack = new MyStack<String>();
		if(testStack.empty()!=true)
			Assert.fail();
	}
	
//	Should create an empty stack of type Integer, and verify correctly that the stack is empty
	@Test
	public void emptyIntegerStackTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		if(testStack.empty()!=true)
			Assert.fail();
	}
	
//	Creates an empty stack of type Integer, push 5 to the top, peek to verify
	@Test
	public void singlePushTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		testStack.push(5);
		if(testStack.peek()!=5)
			Assert.fail();
	}
	
//	Pushes to a stack, then pops it, then verifies that the stack is empty
	@Test
	public void pushPopTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		testStack.push(5);
		testStack.pop();
		if(testStack.empty()!=true)
			Assert.fail();
	}
	
//	Verifies that an EmptyStackException is thrown when attempting to peak an empty Stack
	@Test
	public void emptyPeekTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		try{
			testStack.peek();
		}
		catch(EmptyStackException e)
		{
			return;
		}
		
//		If exception is not thrown, the test fails
		Assert.fail();
	}
	
//	Verifies that an EmptyStackException is thrown when attempting to pop an empty Stack
	@Test
	public void emptyPopTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		try{
			testStack.pop();
		}
		catch(EmptyStackException e)
		{
			return;
		}
		
//		If exception is not thrown, the test fails
		Assert.fail();
	}
	
//	Tests to see that empty returns false when the stack isn't empty
	@Test
	public void notEmptyTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		testStack.push(5);
		if(testStack.empty()==true)
			Assert.fail();
	}
	
//	Tests to see if multiple items are pushed and retrieved by peek correctly
	@Test
	public void multipleItemPushTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		for(int i = 0; i<=5; i++)
		{
			testStack.push(i);
			if(testStack.peek()!=i)
				Assert.fail();
		}
	}
	
	
//	Tests to see if multiple items can be popped, then peeked correctly
	@Test
	public void multipleItemPopTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		for(int i = 0; i<=5; i++)
			testStack.push(i);
		
		testStack.pop();
		testStack.pop();
		
		if(testStack.peek()!=3)
			Assert.fail();
	}
	
//	Checks to see that pop returns the element being removed
	@Test
	public void popReturnTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		testStack.push(5);
		
		if(testStack.pop()!=5)
			Assert.fail();
	}
	
//	Checks to see that push returns the element being added
	@Test
	public void pushReturnTest()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		if(testStack.push(5)!=5)
			Assert.fail();
	}
	
//	Checks to see if the push method can push a null argument without throwing an exception
	@Test
	public void nullItemPush()
	{
		MyStack<Integer> testStack = new MyStack<Integer>();
		try{
			testStack.push(null);
		}
		catch(Exception e)
		{
			Assert.fail();
		}
	}
}
