package project3;

/**
 * Node class of generic type E for use in reference based implementations of data structures
 * 
 * @author Jack Booth
 * @version 4/6/2017
 */
public class Node<E>
{
	private E data;
	private Node<E> next;
	
	/**
	 * Gets node data
	 * @return the data of the node
	 */
	public E getData()
	{
		return data;
	}
	
	/**
	 * gets next node
	 * @return the next node in the data structure
	 */
	public Node<E> getNext()
	{
		return next;
	}
	
	/**
	 * sets the data of the current node
	 * @param data to set
	 */
	public void setData(E data)
	{
		this.data = data;
	}
	
	/**
	 * Changes where the next node is pointing
	 * @param next node to set
	 */
	public void setNext(Node<E> next)
	{
		this.next = next;
	}
}
