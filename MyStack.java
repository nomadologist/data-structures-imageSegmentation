package project3;

import java.util.EmptyStackException;

/**
 * Reference-based implementation of a Stack
 * 
 * @author Jack Booth
 * @version 4/6/2017
 */
public class MyStack<E>
{
	private Node<E> top;
	private int size = 0;
	
	/**
	 * Checks to see if the stack is empty
	 * @return true if the stack is empty, otherwise false.
	 */
	public boolean empty()
	{
		return size == 0;
	}
	
	/**
	 * Peeks at the item at the top of the stack
	 * @return the data at the top of the stack
	 * @throws EmptyStackException if the stack is empty
	 */
	public E peek() throws EmptyStackException
	{
		if(size == 0)
			throw new EmptyStackException();
		
		return top.getData();
	}
	
	/**
	 * Removes the item at the top of the stack and returns it
	 * @return the data of the item at the top of the stack
	 * @throws EmptyStackException if the stack is empty
	 */
	public E pop() throws EmptyStackException
	{
		if(size==0)
			throw new EmptyStackException();
		
		E tmp = top.getData();
		top = top.getNext();
		size--;	
		return tmp;
	}
	
	/**
	 * Pushes an item to the top of the stack
	 * @param item the item to push
	 * @return the item that was pushed
	 */
	public E push(E item)
	{
		Node<E> tmp = top;
		top = new Node<E>();
		top.setData(item);
		top.setNext(tmp);
		size++;
		return item;
	}
}
