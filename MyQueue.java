package project3;

/**
 * Reference-based implementation of a Stack
 * 
 * @author Jack Booth
 * @version 4/6/2017
 */
public class MyQueue<E>
{
	private int size = 0;
	private Node<E> first;
	private Node<E> last;
	
	/**
	 * peeks at the first item in the queue
	 * @return the first item in the queue, or null if the queue is empty
	 */
	public E peek()
	{
		if(size == 0)
			return null;
		return first.getData();
	}
	
	/**
	 * removes the first item in the queue
	 * @return the data removed, or null if the queue is empty
	 */
	public E poll()
	{
		if(size == 0)
			return null;
		E data = first.getData();
		first = first.getNext();
		size--;
		return data;
	}
	
	/**
	 * Adds an item to the back of the queue
	 * @param item to add
	 * @return true if item is successfully added, false if something prevented item from being added
	 * @throws NullPointerException if item is null
	 * @throws ClassCastException if a class is miscast
	 */
	public boolean offer(E item) throws NullPointerException, ClassCastException
	{
		if(item == null)
			throw new NullPointerException();
		
		Node<E> tmp = last;
		last = new Node<E>();
		last.setData(item);
		last.setNext(null);
		if(size == 0)
			first = last;
		else
			tmp.setNext(last);
		size++;
		return true;
	}
}
