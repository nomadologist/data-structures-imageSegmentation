package project3;

import org.junit.Test;
import org.junit.Assert;

/**
 * JUnit tests for the MyQueue class.
 * 
 * @author Jack Booth
 * @version 4/6/2017
 */
public class MyQueueTest
{
//	Checks to make sure the constructor properly creates a queue object
	@Test
	public void nullQueueTest()
	{
		MyQueue<String> testQueue = new MyQueue<String>();
		if(testQueue==null)
			Assert.fail();
	}
	
//	Checks to make sure that when the queue is empty, peek returns a null value
	@Test
	public void emptyQueuePeekTest()
	{
		MyQueue<Integer> testQueue = new MyQueue<Integer>();
		if(testQueue.peek()!=null)
			Assert.fail();
	}
	
//	Checks to make sure a single value is enqued correctly
	@Test
	public void singleOfferTest()
	{
		MyQueue<Integer> testQueue = new MyQueue<Integer>();
		testQueue.offer(5);
		if(testQueue.peek()!=5)
			Assert.fail();
	}
	
//	Checks to make sure null offer throws an exception
	@Test
	public void nullOfferTest()
	{
		MyQueue<Integer> testQueue = new MyQueue<Integer>();
		try{
			testQueue.offer(null);
		}
		catch(NullPointerException e)
		{
			return;
		}
		
		Assert.fail();
	}
	
//	Checks to see if items are enqued in FIFO order
	@Test
	public void multipleOfferTest()
	{
		MyQueue<Integer> testQueue = new MyQueue<Integer>();
		testQueue.offer(1);
		testQueue.offer(2);
		testQueue.offer(3);
		if(testQueue.peek()!=1)
			Assert.fail();
		
		testQueue.poll();
		
		if(testQueue.peek()!=2)
			Assert.fail();
		
		testQueue.poll();
		
		if(testQueue.peek()!=3)
			Assert.fail();
	}
	
//	Checks to see if offer returns true after successful enqueue
	@Test
	public void offerReturnTest()
	{
		MyQueue<Integer> testQueue = new MyQueue<Integer>();
		if(testQueue.offer(5)!=true)
			Assert.fail();
	}
	
//	Checks to see if poll returns the item being dequeued
	@Test
	public void pollReturnTest()
	{
		MyQueue<Integer> testQueue = new MyQueue<Integer>();
		testQueue.offer(5);
		if(testQueue.poll()!=5)
			Assert.fail();
	}
	
//	Checks to see if poll returns null when the queue is empty
	@Test
	public void emptyQueuePollTest()
	{
		MyQueue<Integer> testQueue = new MyQueue<Integer>();
		if(testQueue.poll()!=null)
			Assert.fail();
	}
	
//	Enqueues then dequeues, then checks to make sure the queue is empty
	@Test
	public void offerPollTest()
	{
		MyQueue<Integer> testQueue = new MyQueue<Integer>();
		testQueue.offer(5);
		testQueue.poll();
		if(testQueue.peek()!=null)
			Assert.fail();
	}
}
